package com.microspringsec.microspringsecurity.model;


import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private int roleId;

    @Column(name = "role")
    private String role;

    @Column(name = "description")
    private String description;


    public int getRoleId(){
        return roleId;
    }
    public void setRoleId(int roleId){
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Role() {
    }
    public Role(Integer roleId, String role, String description) {
        this.roleId = roleId;
        this.role = role;
        this.description = description;
    }
}
