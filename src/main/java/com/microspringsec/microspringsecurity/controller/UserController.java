package com.microspringsec.microspringsecurity.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest/users")
@RestController
public class UserController {

    @GetMapping("/all")
    public String users(){
        return "All user";
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/secured/all")
    public String securedUsers(){
        return "User secured";
    }

    @GetMapping("/secured/alternative")
    public String alternative(){
        return "Alternative";
    }
}
