package com.microspringsec.microspringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrospringsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrospringsecurityApplication.class, args);
	}
}
