package com.microspringsec.microspringsecurity.service;

import com.microspringsec.microspringsecurity.dao.UserRepository;
import com.microspringsec.microspringsecurity.model.CustomUserDetails;
import com.microspringsec.microspringsecurity.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustumUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> optionalUser = userRepository.findByName(username);

        optionalUser
                .orElseThrow(() -> new UsernameNotFoundException("Username Not Found"));
        return optionalUser
                .map(CustomUserDetails::new).get();

    }
}
